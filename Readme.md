[![Multi-Binder-By-McFree v2.6](https://img.shields.io/badge/download-v2.6-brightgreen.svg?style=flat)](https://bitbucket.org/mfsoftru/multi-binder/get/version_2.6.zip)
[![Multi-Binder-By-McFree v3.0](https://img.shields.io/badge/download-v3.0-brightgreen.svg?style=flat)](https://bitbucket.org/mfsoftru/multi-binder/get/version_3.0.zip)

#Multi-Binder By McFree
=====================

[Multi-Binder By McFree](http://mf-soft.ru/index.php?act=soft&id=4) Специальный мультистрочный биндер для SA-MP RP серверов и подобных.

## Разработка
При разработке программы использовался компилируемый скриптовый язык AutoHotKey

Все актуальные версии хранятся в ветках с названиями версий

## Возможности Multi-Binder
1. Полная настройка биндера под себя (Настройка хоткеев, дизайна, нажатия кнопки Enter и т.д)
2. Самый удобный редактор хоткеев среди всех биндеров.
3. Все настройки Биндера хранятся в ini файле в папке с биндером.
4. Все строки хоткеев вы можете так же изменять путем выбора txt файла (замена строк на строки txt файла).
5. Все строки хоткеев хранятся в папке BindsText.
6. Имеется настройка хоткея паузы и перезапуска Биндера.
7. Проверка версий биндера с последующим окном скачивания (при необходимости)
8. Вы можете создать свои профили под свой вкус и функционал
9. Возможность включать Биндер в свернутом режиме
10. Возможность включать при запуске Windows
11. Возможность Отключать оповещение обновлений

## Предупреждения
первые версии Multi-Binder был написан левой рукой на правой коленке и по этому прошу с пониманием относиться к стилю написания кода. Последующие версии (начиная с 3.0) будут писаться по принципу ООП при помощи классов и модульности


##Слова разработчика:
Долго мучаясь с Биндерами хотелось облегчить себе задачу, создав скриптовый биндер (без графического окна) пользовался им, был очень полезен при моих то обязанностях (лекции, статичные фразы и т.д). После понял, что стоит помочь и другим, по-этому разработал графический интерфейс специально для удобства и написания биндов без знаний программирования. Среди знакомых программа начала пользоваться очень высокой популярностью и после этого я решил выпустить Мульти-Биндер в свет с его последующими обновлениями. Собственно последняя версия на данный момент представлена вам! Пользуйтесь на здоровье!

##P.S:
Если имеются идеи для дальнейших обновлений Биндера то слушаю вас, помогайте его улучшению! =)
Если возникнут вопросы то пишите их мне в ЛС, по мере развития (и популярности) Биндер будет фикситься и дополняться функционалом.

##Ссылки на скачивание при помощи Yandex Disk
* [Ссылка на скачивания архива (ручная установка) v 2.3.5](https://yadi.sk/d/YcrnvjJtr74Ze)
* [Ссылка на скачивание установщика v 2.3.5](https://yadi.sk/d/5ASDJ17WrExEN)
